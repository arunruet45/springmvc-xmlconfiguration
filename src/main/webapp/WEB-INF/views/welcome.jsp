<%--
  Created by IntelliJ IDEA.
  User: arunkundu
  Date: 11/17/19
  Time: 2:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

    <title>Intertnationalization</title>
</head>
<body>

<a id="gr" href="/intn?lang=gr">German</a>&nbsp;&nbsp;&nbsp;
<a id="en" href="/intn?lang=en">English</a>&nbsp;&nbsp;&nbsp;
<a id="fr" href="/intn?lang=fr">French</a>&nbsp;&nbsp;&nbsp;


<a href="/logging">LOGGING</a>

<a href="/findAll">View All</a> <br>
<a href="/person/create">ADD NEW PERSON</a><br><br>

<h3>${welcomeMsg}</h3>
<h4><spring:message code="welcome.message"/></h4>
</body>
</html>
