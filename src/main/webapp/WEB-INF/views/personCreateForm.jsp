<%--
  Created by IntelliJ IDEA.
  User: arunkundu
  Date: 11/18/19
  Time: 4:20 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>

<html>
<head>
    <title>Title</title>
</head>
<body>

<form:form method="post" modelAttribute="person">

    <label>First name</label>
    <form:input path="firstName"/>
    <label>Last name</label>
    <form:input path="lastName"/>
    <label>Age</label>
    <form:input path="age"/>
    <input type="submit" name="submit" value="SUBMIT">
</form:form>

</body>
</html>
