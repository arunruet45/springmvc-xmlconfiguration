
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<body>
<h2>Hello World!</h2>



<a id="gr" href="?lang=gr">German</a>&nbsp;&nbsp;&nbsp;
<a id="en" href="?lang=en">English</a>&nbsp;&nbsp;&nbsp;
<a id="fr" href="?lang=fr">French</a>&nbsp;&nbsp;
<a href="/logging">LOGGING</a>

<a href="/findAll">View All</a> <br>
<a href="/person/create">ADD NEW PERSON</a><br><br>

<h3>${welcomeMsg}</h3>
<h4><spring:message code="welcome.message"/></h4>
</body>
</html>

<%--<%--%>
<%--    request.getRequestDispatcher("/intn").forward(request, response);--%>
<%--%>--%>