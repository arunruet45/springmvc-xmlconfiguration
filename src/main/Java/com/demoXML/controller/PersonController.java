//package com.demoXML.controller;
//
//import com.demoXML.entity.Person;
//import com.demoXML.rest.exceptions.custom.WiredAgeException;
//import com.demoXML.service.UserService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//
//@Controller
//public class PersonController{
//
//    @Autowired
//    @Qualifier("personServiceImpl")
//    private UserService personService;
//
//    @RequestMapping(value = "/person/create")
//    public String personCreateForm(Model model){
//        model.addAttribute("person", new Person());
//        return "personCreateForm";
//    }
//    @RequestMapping(value = "/person/create", method = RequestMethod.POST)
//    public String personCreate(@ModelAttribute("person") Person person) {
//        try {
//            if (person.getAge()>200){
//                throw new WiredAgeException("adsds");
//            }
//            personService.save(person);
//            return "redirect:/findAll";
//        }
//        catch(WiredAgeException re){
//            re.getCause();
//            return null;
//        }
//    }
//
//    @RequestMapping(value = "/findOne/{id}", method = RequestMethod.GET)
//    public Person findOne(@PathVariable int id){
//        return personService.findOne(id);
//    }
//
//    @RequestMapping(value = "/person/update/{id}", method = RequestMethod.GET)
//    public String personUpdateForm(@PathVariable int id, Model model) {
//        Person person = personService.findOne(id);
//        model.addAttribute("command",person);
//        return "personUpdateForm";
//    }
//
//    @RequestMapping(value = "/person/update/{id}", method = RequestMethod.POST)
//    public String personUpdate(@ModelAttribute("person") Person person){
//        personService.update(person);
//        return "redirect:/findAll";
//    }
//
//    @RequestMapping(value = "/person/delete/{id}", method = RequestMethod.GET)
//    public String delete(@PathVariable int id) {
//        personService.delete(id);
//        return "redirect:/findAll";
//    }
//
//    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
//    public String findAll(Model model) {
//
//        List<Person> persons = personService.findAll();
//        model.addAttribute("persons", persons);
//        return "viewAll";
//    }
//}
