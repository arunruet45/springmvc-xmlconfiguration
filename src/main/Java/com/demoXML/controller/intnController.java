package com.demoXML.controller;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.SpringServletContainerInitializer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class intnController {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView intView(){
        ModelAndView mv = new ModelAndView();
        mv.addObject("welcomeMsg", "Spring MVC internationalization practice");
        mv.setViewName("index");
        return mv;
    }
}
