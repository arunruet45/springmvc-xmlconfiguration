package com.demoXML.controller;

import com.demoXML.log4jAdditivity.bar.BarBean;
import com.demoXML.log4jAdditivity.foo.FooBean;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class loggingController {

    private static final Logger logger = Logger.getLogger(loggingController.class);

    @RequestMapping("/logging")
    public void mainLogger(){

        logger.debug("hello debug from mainLogging class");

        FooBean fooBean = new FooBean();
        BarBean barBean = new BarBean();

        fooBean.sayHello();
        barBean.sayHello();

    }
}
