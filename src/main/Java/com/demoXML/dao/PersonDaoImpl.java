package com.demoXML.dao;

import com.demoXML.entity.AngularPerson;
import com.demoXML.entity.Person;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("personDaoImpl")
public class PersonDaoImpl implements UserDao {
    private Configuration config = new Configuration().configure().addAnnotatedClass(AngularPerson.class);
    private SessionFactory sf = config.buildSessionFactory();
    private Session session = sf.openSession();

    @Override
    public void save(AngularPerson person) {

        Transaction tx = session.beginTransaction();
        session.save(person);
        tx.commit();
    }

    @Override
    public void update(Person person) {
//
//        try {
//            Transaction tx = session.beginTransaction();
//            Person person1 = session.get(Person.class, person.getId());
//            person1.setAge(person.getAge());
//            person1.setFirstName(person.getFirstName());
//            person1.setLastName(person.getLastName());
//            session.update(person1);
//            tx.commit();
//        } catch (Exception e) {
//            System.out.println("Bad Request");
//        }
    }

    @Override
    public void delete(int personId) {
//        Transaction tx = session.beginTransaction();
//        Person person = (Person) session.get(Person.class, personId);
//        session.delete(person);
//        tx.commit();
    }

    @Override
    public AngularPerson findOne(int personId) {
        Transaction tx = session.beginTransaction();
        AngularPerson person = session.get(AngularPerson.class, personId);
        tx.commit();
        return person;
         // return null;
    }

    @Override
    public List<Person> findAll() {

//        Transaction tx = session.beginTransaction();
//        List<Person> persons = session.createQuery("from Person", Person.class).list();
//        tx.commit();
//        return persons;
        return null;
    }
}
