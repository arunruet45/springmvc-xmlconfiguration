package com.demoXML.dao;

import com.demoXML.entity.AngularPerson;
import com.demoXML.entity.Person;

import java.util.List;

public interface UserDao {

    void save(AngularPerson person);
    void update(Person person);
    void delete(int personId);
    AngularPerson findOne(int personId);
    List<Person> findAll();
}
