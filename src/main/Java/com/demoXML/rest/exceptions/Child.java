package com.demoXML.rest.exceptions;

public class Child {
    private int code;
    private String message;
    private Throwable description;
    private String link;

    public int getCode(int i) {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getDescription() {
        return description;
    }

    public String getLink() {
        return link;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setDescription(Throwable description) {
        this.description = description;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
