package com.demoXML.rest.exceptions.custom;

public class WiredAgeException extends RuntimeException {
    public WiredAgeException(String message) {
        super(message);
    }
}
