package com.demoXML.rest.exceptions;

public class Parent {
    private int status;
    private Child child;

    public int getStatus() {
        return status;
    }

    public Child getChild() {
        return child;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setChild(Child child) {
        this.child = child;
    }
}
