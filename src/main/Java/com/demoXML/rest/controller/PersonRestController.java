package com.demoXML.rest.controller;

import com.demoXML.entity.AngularPerson;
import com.demoXML.entity.Person;
import com.demoXML.rest.exceptions.Child;
import com.demoXML.rest.exceptions.Parent;
import com.demoXML.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequestWrapper;
import javax.servlet.http.*;
import java.io.Console;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/")
public class PersonRestController {

    @Autowired
    @Qualifier("personServiceImpl")
    private UserService personService;

    private ObjectMapper mapper = new ObjectMapper();


    @RequestMapping(value = "v1/person/create", method = RequestMethod.POST)
    public ResponseEntity<Object> personCreate(@RequestBody AngularPerson person, HttpServletRequest req,  HttpServletResponse resp) throws JsonProcessingException {
//        try {
//            if (person.getPassword()>200){
//                throw new WiredAgeException("Wired Age");
//            }
            personService.save(person);
            return new ResponseEntity<Object>(person, HttpStatus.OK);
            //return "redirect:/findAll";
//        }
//        catch(WiredAgeException we){
//            Parent parentErrors = new Parent();
//            Child childErrors = new Child();
//            parentErrors.setStatus(resp.SC_NOT_ACCEPTABLE);
//            parentErrors.setChild(childErrors);
//            childErrors.setCode(45);
//            childErrors.setMessage(we.getMessage());
//            childErrors.setDescription(we.getCause());
//            childErrors.setLink("Support Link");
//            return mapper.writeValueAsString(parentErrors);
//        }
    }

    @RequestMapping(value = "v1/findOne/{id}", method = RequestMethod.GET)
    public String findOne(@PathVariable int id, HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException {

        try {
            return mapper.writeValueAsString(personService.findOne(id));
        }
        catch (RuntimeException re){
            Parent parentErrors = new Parent();
            Child childErrors = new Child();
            parentErrors.setStatus(resp.SC_BAD_REQUEST);
            parentErrors.setChild(childErrors);
            childErrors.setCode(45);
            childErrors.setMessage(re.getMessage());
            childErrors.setDescription(re.getCause());
            childErrors.setLink("Support Link");
            return mapper.writeValueAsString(parentErrors);
        }
    }

    @RequestMapping(value = "v1/person/update/{id}", method = RequestMethod.GET)
    public String personUpdateForm(@PathVariable int id, Model model) {
        AngularPerson person = personService.findOne(id);
        model.addAttribute("command",person);
        return "personUpdateForm";
    }

    @RequestMapping(value = "v1/person/update/{id}", method = RequestMethod.POST)
    public String personUpdate(@ModelAttribute("person") com.demoXML.entity.Person person){
        personService.update(person);
        return "redirect:/findAll";
    }

    @RequestMapping(value = "v1/person/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable int id) {
        personService.delete(id);
        return "redirect:/findAll";
    }

    @RequestMapping(value = "v1/findAll", method = RequestMethod.GET)
    public String findAll(Model model, HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException {
        try{
            resp.setStatus(2000);
            List<Person> persons = personService.findAll();
            model.addAttribute("persons", persons);
            return mapper.writeValueAsString(persons);

        }
        catch (RuntimeException re){

            Parent parentErrors = new Parent();
            Child childErrors = new Child();
            parentErrors.setStatus(resp.SC_CONFLICT);
            parentErrors.setChild(childErrors);
            childErrors.setCode(45);
            childErrors.setMessage(re.getMessage());
            childErrors.setDescription(re.getCause());
            childErrors.setLink("Support Link");
            return mapper.writeValueAsString(parentErrors);
        }
    }
}
