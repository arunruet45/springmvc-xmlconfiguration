package com.demoXML.service;

import com.demoXML.entity.AngularPerson;
import com.demoXML.entity.Person;

import java.util.List;

public interface UserService {

    void save(AngularPerson person);
    void update(Person person);
    void delete(int personId);
    AngularPerson findOne(int personId);
    List<Person> findAll();
}
