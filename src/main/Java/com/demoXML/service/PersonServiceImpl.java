package com.demoXML.service;


import com.demoXML.dao.UserDao;
import com.demoXML.entity.AngularPerson;
import com.demoXML.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("personServiceImpl")
public class PersonServiceImpl implements UserService {

    @Autowired
    @Qualifier("personDaoImpl")
    private UserDao personDao;

    @Override
    public void save(AngularPerson person) { personDao.save(person); }

    @Override
    public void update(Person person) {
        personDao.update(person);
    }

    @Override
    public void delete(int personId) {
        personDao.delete(personId);
    }

    @Override
    public AngularPerson findOne(int personId) {
       return personDao.findOne(personId);
    }

    @Override
    public List<Person> findAll() {
        return personDao.findAll();
    }
}
