package com.demoXML.log4jAdditivity.foo;


import org.apache.log4j.Logger;

public class FooBean {

    private static final Logger logger = Logger.getLogger(FooBean.class);

    public void sayHello(){
        logger.debug("hello debug from FooBean class");
        logger.info("hello info from FooBean class");
    }

}
