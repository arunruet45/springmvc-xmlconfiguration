package com.demoXML.log4jAdditivity.bar;

import org.apache.log4j.Logger;

public class BarBean {

    private static final Logger logger = Logger.getLogger(BarBean.class);

    public void sayHello(){
        logger.info("hello info from BarBean class");
        logger.warn("hello warn from barBean class");
    }
}
