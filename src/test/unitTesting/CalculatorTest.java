package unitTesting;

import com.demoXML.demoTesting.Calculator;
import com.demoXML.demoTesting.CalculatorService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.mockito.Mockito.*;

public class CalculatorTest {

    Calculator c = null;
    // manual
    private CalculatorService service = mock(CalculatorService.class);
    // If I want to use annotation based then I have to mention this below rule. Thats mention that I am using Junit and mockito together.
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();
    // Annotation Based
    @Mock
    private CalculatorService s;

    @Before
    public void setUp(){
        c = new Calculator(service);
    }
    @Test
    public void addTest(){
        when(service.add(2,3)).thenReturn(5);
        Assert.assertEquals(10, c.perform(2,3));
        verify(service).add(2,3);
    }
}
